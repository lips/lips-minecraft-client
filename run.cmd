start ^
javaw ^
-Dos.name="Windows 10" ^
-Dos.version=10.0 ^
-Xmn128M ^
-Xmx4096M ^
-Djava.library.path=files\versions\1.7.10\natives ^
-cp ^
files\libraries\com\mojang\netty\1.6\netty-1.6.jar;^
files\libraries\com\mojang\realms\1.3.5\realms-1.3.5.jar;^
files\libraries\org\apache\commons\commons-compress\1.8.1\commons-compress-1.8.1.jar;^
files\libraries\org\apache\httpcomponents\httpclient\4.3.3\httpclient-4.3.3.jar;^
files\libraries\commons-logging\commons-logging\1.1.3\commons-logging-1.1.3.jar;^
files\libraries\org\apache\httpcomponents\httpcore\4.3.2\httpcore-4.3.2.jar;^
files\libraries\java3d\vecmath\1.3.1\vecmath-1.3.1.jar;^
files\libraries\net\sf\trove4j\trove4j\3.0.3\trove4j-3.0.3.jar;^
files\libraries\com\ibm\icu\icu4j-core-mojang\51.2\icu4j-core-mojang-51.2.jar;^
files\libraries\net\sf\jopt-simple\jopt-simple\4.5\jopt-simple-4.5.jar;^
files\libraries\com\paulscode\codecjorbis\20101023\codecjorbis-20101023.jar;^
files\libraries\com\paulscode\codecwav\20101023\codecwav-20101023.jar;^
files\libraries\com\paulscode\libraryjavasound\20101123\libraryjavasound-20101123.jar;^
files\libraries\com\paulscode\librarylwjglopenal\20100824\librarylwjglopenal-20100824.jar;^
files\libraries\com\paulscode\soundsystem\20120107\soundsystem-20120107.jar;^
files\libraries\io\netty\netty-all\4.0.10.Final\netty-all-4.0.10.Final.jar;^
files\libraries\com\google\guava\guava\15.0\guava-15.0.jar;^
files\libraries\org\apache\commons\commons-lang3\3.1\commons-lang3-3.1.jar;^
files\libraries\commons-io\commons-io\2.4\commons-io-2.4.jar;^
files\libraries\commons-codec\commons-codec\1.9\commons-codec-1.9.jar;^
files\libraries\net\java\jinput\jinput\2.0.5\jinput-2.0.5.jar;^
files\libraries\net\java\jutils\jutils\1.0.0\jutils-1.0.0.jar;^
files\libraries\com\google\code\gson\gson\2.2.4\gson-2.2.4.jar;^
files\libraries\com\mojang\authlib\1.5.21\authlib-1.5.21.jar;^
files\libraries\org\apache\logging\log4j\log4j-api\2.0-beta9\log4j-api-2.0-beta9.jar;^
files\libraries\org\apache\logging\log4j\log4j-core\2.0-beta9\log4j-core-2.0-beta9.jar;^
files\libraries\org\lwjgl\lwjgl\lwjgl\2.9.1\lwjgl-2.9.1.jar;^
files\libraries\org\lwjgl\lwjgl\lwjgl_util\2.9.1\lwjgl_util-2.9.1.jar;^
files\libraries\tv\twitch\twitch\5.16\twitch-5.16.jar;^
files\versions\1.7.10\1.7.10.jar ^
-XX:+UseConcMarkSweepGC ^
-Dfml.ignoreInvalidMinecraftCertificates=true ^
-Dfml.ignorePatchDiscrepancies=true ^
net.minecraft.client.main.Main ^
--username Player ^
--version 1.7.10 ^
--gameDir files ^
--assetsDir files\assets ^
--assetIndex 1.7.10 ^
--uuid 0dfbca3eba8d11e891b8002590a1379b ^
--accessToken 2fc5043f-0cd7-4d65-99ff-8a571965f2ae ^
--userProperties {} ^
--userType mojang ^
--width 925 ^
--height 530
