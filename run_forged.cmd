start ^
javaw ^
-Dos.name="Windows 10" ^
-Dos.version=10.0 ^
-Xmn128M ^
-Xmx4096M ^
-Djava.library.path=files\versions\1.7.10\natives ^
-cp ^
files\libraries\com\mumfrey\liteloader\1.7.10\liteloader-1.7.10.jar;^
files\libraries\net\minecraftforge\forge\1.7.10-10.13.4.1614-1.7.10\forge-1.7.10-10.13.4.1614-1.7.10.jar;^
files\libraries\net\minecraft\launchwrapper\1.12\launchwrapper-1.12.jar;^
files\libraries\org\ow2\asm\asm-all\5.0.3\asm-all-5.0.3.jar;^
files\libraries\com\typesafe\akka\akka-actor_2.11\2.3.3\akka-actor_2.11-2.3.3.jar;^
files\libraries\com\typesafe\config\1.2.1\config-1.2.1.jar;^
files\libraries\org\scala-lang\scala-actors-migration_2.11\1.1.0\scala-actors-migration_2.11-1.1.0.jar;^
files\libraries\org\scala-lang\scala-compiler\2.11.1\scala-compiler-2.11.1.jar;^
files\libraries\org\scala-lang\plugins\scala-continuations-library_2.11\1.0.2\scala-continuations-library_2.11-1.0.2.jar;^
files\libraries\org\scala-lang\plugins\scala-continuations-plugin_2.11.1\1.0.2\scala-continuations-plugin_2.11.1-1.0.2.jar;^
files\libraries\org\scala-lang\scala-library\2.11.1\scala-library-2.11.1.jar;^
files\libraries\org\scala-lang\scala-parser-combinators_2.11\1.0.1\scala-parser-combinators_2.11-1.0.1.jar;^
files\libraries\org\scala-lang\scala-reflect\2.11.1\scala-reflect-2.11.1.jar;^
files\libraries\org\scala-lang\scala-swing_2.11\1.0.1\scala-swing_2.11-1.0.1.jar;^
files\libraries\org\scala-lang\scala-xml_2.11\1.0.2\scala-xml_2.11-1.0.2.jar;^
files\libraries\lzma\lzma\0.0.1\lzma-0.0.1.jar;^
files\libraries\com\google\guava\guava\17.0\guava-17.0.jar;^
files\libraries\com\mojang\netty\1.6\netty-1.6.jar;^
files\libraries\com\mojang\realms\1.3.5\realms-1.3.5.jar;^
files\libraries\org\apache\commons\commons-compress\1.8.1\commons-compress-1.8.1.jar;^
files\libraries\org\apache\httpcomponents\httpclient\4.3.3\httpclient-4.3.3.jar;^
files\libraries\commons-logging\commons-logging\1.1.3\commons-logging-1.1.3.jar;^
files\libraries\org\apache\httpcomponents\httpcore\4.3.2\httpcore-4.3.2.jar;^
files\libraries\java3d\vecmath\1.3.1\vecmath-1.3.1.jar;^
files\libraries\net\sf\trove4j\trove4j\3.0.3\trove4j-3.0.3.jar;^
files\libraries\com\ibm\icu\icu4j-core-mojang\51.2\icu4j-core-mojang-51.2.jar;^
files\libraries\net\sf\jopt-simple\jopt-simple\4.5\jopt-simple-4.5.jar;^
files\libraries\com\paulscode\codecjorbis\20101023\codecjorbis-20101023.jar;^
files\libraries\com\paulscode\codecwav\20101023\codecwav-20101023.jar;^
files\libraries\com\paulscode\libraryjavasound\20101123\libraryjavasound-20101123.jar;^
files\libraries\com\paulscode\librarylwjglopenal\20100824\librarylwjglopenal-20100824.jar;^
files\libraries\com\paulscode\soundsystem\20120107\soundsystem-20120107.jar;^
files\libraries\io\netty\netty-all\4.0.10.Final\netty-all-4.0.10.Final.jar;^
files\libraries\org\apache\commons\commons-lang3\3.3.2\commons-lang3-3.3.2.jar;^
files\libraries\commons-io\commons-io\2.4\commons-io-2.4.jar;^
files\libraries\commons-codec\commons-codec\1.9\commons-codec-1.9.jar;^
files\libraries\net\java\jinput\jinput\2.0.5\jinput-2.0.5.jar;^
files\libraries\net\java\jutils\jutils\1.0.0\jutils-1.0.0.jar;^
files\libraries\com\google\code\gson\gson\2.2.4\gson-2.2.4.jar;^
files\libraries\com\mojang\authlib\1.5.21\authlib-1.5.21.jar;^
files\libraries\org\apache\logging\log4j\log4j-api\2.0-beta9\log4j-api-2.0-beta9.jar;^
files\libraries\org\apache\logging\log4j\log4j-core\2.0-beta9\log4j-core-2.0-beta9.jar;^
files\libraries\org\lwjgl\lwjgl\lwjgl\2.9.1\lwjgl-2.9.1.jar;^
files\libraries\org\lwjgl\lwjgl\lwjgl_util\2.9.1\lwjgl_util-2.9.1.jar;^
files\libraries\tv\twitch\twitch\5.16\twitch-5.16.jar;^
files\versions\1.7.10\1.7.10.jar ^
-XX:+UseConcMarkSweepGC ^
-Dfml.ignoreInvalidMinecraftCertificates=true ^
-Dfml.ignorePatchDiscrepancies=true ^
net.minecraft.launchwrapper.Launch ^
--username Player ^
--version 1.7.10 ^
--gameDir files ^
--assetsDir files\assets ^
--assetIndex 1.7.10 ^
--uuid 0dfbca3eba8d11e891b8002590a1379b ^
--accessToken 25a5d3bb-701a-42b4-a316-a58dcd85347c ^
--userProperties {} ^
--userType mojang ^
--tweakClass com.mumfrey.liteloader.launch.LiteLoaderTweaker ^
--tweakClass cpw.mods.fml.common.launcher.FMLTweaker ^
--width 925 ^
--height 530
