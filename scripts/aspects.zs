import mods.thaumcraft.Aspects;

Aspects.add(<chisel:marble>, "terra 2");
Aspects.add(<chisel:diorite>, "terra 2");
Aspects.add(<chisel:limestone>, "terra 2");

Aspects.add(<BiomesOPlenty:rocks>, "terra 2");
Aspects.add(<BiomesOPlenty:newBopGrass>, "herba 1, terra 1");
Aspects.add(<BiomesOPlenty:newBopDirt>, "terra 2");
