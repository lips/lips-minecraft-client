game.setLocalization("tile.sink.name", "Sink");
game.setLocalization("entity.ThaumicHorizons.Nightmare.name", "Nightmare");
game.setLocalization("tc.research_name.TB.TaintMinor", "Unknown Research");
game.setLocalization("tc.research_text.TB.TaintMinor", "How did it get there?");
game.setLocalization("tb.rec.minorTaint.page.0", "The essence of this research was lost in the centuries...");
game.setLocalization("item.railcraft.part.plate.lead.name", "Lead Plate");
game.setLocalization("item.null.name", "Fusion Focus");
game.setLocalization("techno:existenceSealer.name", "Existence Sealer");
game.setLocalization("tile.techno:reservoir.name", "Eldritch Reservoir");
game.setLocalization("tc.research_page.MB_FrameMagic", "The description of this item is erased from the book...");
game.setLocalization("tile.ThaumicHorizons_vatInterior.name", "Curative Vat Interior");
game.setLocalization("entity.Forestry.butterflyGE.name", "Butterfly");
game.setLocalization("entity.ItemFrame.name", "Item Frame");

game.setLocalization("tile.TFTrophy.0.name", "Hydra Trophy");
game.setLocalization("tile.TFTrophy.1.name", "Naga Trophy");
game.setLocalization("tile.TFTrophy.2.name", "Lich Trophy");
game.setLocalization("tile.TFTrophy.3.name", "Ur-ghast Trophy");
game.setLocalization("tile.TFTrophy.4.name", "Snow Queen Trophy");
