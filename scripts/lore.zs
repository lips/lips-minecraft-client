<WarpTheory:item.warptheory.cleanser>.addTooltip(format.green("This little thing can completely clear warp from you."));
<WarpTheory:item.warptheory.cleanser>.addTooltip(format.red("All removed warp will be released from your mind to outer space and continue to haunt you until you dead."));
