import mods.nei.NEI;

NEI.hide(<minecraft:water>);
NEI.hide(<minecraft:flowing_water>);
NEI.hide(<minecraft:lava>);
NEI.hide(<minecraft:flowing_lava>);
NEI.hide(<minecraft:fire>);
NEI.hide(<minecraft:portal>);
NEI.hide(<minecraft:mob_spawner>);
NEI.hide(<minecraft:end_portal>);
NEI.hide(<minecraft:spawn_egg:*>);

NEI.hide(<BiomesOPlenty:poison>);
NEI.hide(<BiomesOPlenty:honey>);
NEI.hide(<BiomesOPlenty:hell_blood>);

NEI.hide(<Forestry:fluid.bioethanol>);
NEI.hide(<Forestry:fluid.biomass>);
NEI.hide(<Forestry:fluid.glass>);
NEI.hide(<Forestry:fluid.honey>);
NEI.hide(<Forestry:fluid.ice>);
NEI.hide(<Forestry:fluid.juice>);
NEI.hide(<Forestry:fluid.milk>);
NEI.hide(<Forestry:fluid.seedoil>);
NEI.hide(<Forestry:fluid.short.mead>);
NEI.hide(<Forestry:fluid.mead>);

NEI.hide(<ForgeMicroblock:microblock:*>);

NEI.hide(<Growthcraft:grccore.BlockFluidSaltWater>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.0>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.1>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.2>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.3>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.4>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.5>);
NEI.hide(<Growthcraft|Apples:grc.appleCiderFluid.6>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.0>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.1>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.2>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.3>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.4>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.5>);
NEI.hide(<Growthcraft|Bees:grc.honeyMeadFluid.6>);
NEI.hide(<Growthcraft|Bees:grc.BlockFluidHoney>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.0>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.1>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.2>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.3>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.4>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.5>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.6>);
NEI.hide(<Growthcraft|Grapes:grc.grapeWineFluid.7>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.0>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.1>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.2>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.3>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.4>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.5>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.6>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.7>);
NEI.hide(<Growthcraft|Hops:grc.hopAleFluid.8>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.0>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.1>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.2>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.3>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.4>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.5>);
NEI.hide(<Growthcraft|Hops:grc.lagerFluid.6>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidMilk>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidButterMilk>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidCream>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidCurds>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidRennet>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidWhey>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidSkimMilk>);
NEI.hide(<Growthcraft|Milk:grcmilk.BlockFluidPasteurizedMilk>);
NEI.hide(<Growthcraft|Milk:grcmilk.KumisFluid.0>);
NEI.hide(<Growthcraft|Milk:grcmilk.KumisFluid.1>);
NEI.hide(<Growthcraft|Milk:grcmilk.KumisFluid.2>);
NEI.hide(<Growthcraft|Milk:grcmilk.KumisFluid.3>);
NEI.hide(<Growthcraft|Milk:grcmilk.KumisFluid.4>);
NEI.hide(<Growthcraft|Milk:grcmilk.KumisFluid.5>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.0>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.1>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.2>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.3>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.4>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.5>);
NEI.hide(<Growthcraft|Rice:grc.riceSakeFluid.6>);

NEI.hide(<Railcraft:fluid.creosote>);
NEI.hide(<Railcraft:fluid.steam>);
NEI.hide(<Railcraft:machine.alpha:*>);
NEI.hide(<Railcraft:firestone.recharge>);

NEI.hide(<Thaumcraft:blockFluxGoo>);
NEI.hide(<Thaumcraft:blockFluxGas>);
NEI.hide(<Thaumcraft:blockFluidPure>);
NEI.hide(<Thaumcraft:blockFluidDeath>);
NEI.hide(<Thaumcraft:blockArcaneDoor>);
NEI.hide(<Thaumcraft:blockArcaneFurnace>);
NEI.hide(<Thaumcraft:blockAlchemyFurnace>);
NEI.hide(<Thaumcraft:blockManaPod>);
NEI.hide(<Thaumcraft:blockWarded>);
NEI.hide(<Thaumcraft:blockHole>);
NEI.hide(<Thaumcraft:blockPortalEldritch>);
NEI.hide(<Thaumcraft:blockEldritchNothing>);
NEI.hide(<Thaumcraft:ItemThaumonomicon:42>);
NEI.hide(<Thaumcraft:ItemSpawnerEgg:*>);

NEI.hide(<ThaumicHorizons:vat>);
NEI.hide(<ThaumicHorizons:vatSolid>);
NEI.hide(<ThaumicHorizons:vatInterior>);
NEI.hide(<ThaumicHorizons:light>);
NEI.hide(<ThaumicHorizons:lightSolar>);
NEI.hide(<ThaumicHorizons:portalTH>);
NEI.hide(<ThaumicHorizons:gatewayTH>);
NEI.hide(<ThaumicHorizons:spawnerEgg:*>);
NEI.hide(<ThaumicHorizons:syringeInjection:*>);
NEI.hide(<ThaumicHorizons:syringeBloodSample:*>);
NEI.hide(<ThaumicHorizons:infusionCheat:*>);
NEI.hide(<ThaumicHorizons:infusionSelfCheat:*>);
NEI.hide(<ThaumicHorizons:golemPlacer:*>);
NEI.hide(<ThaumicHorizons:nodeCheat>);
NEI.hide(<ThaumicHorizons:dummy>);
NEI.hide(<ThaumicHorizons:dummyVat>);

NEI.hide(<TwilightForest:tile.TFPortal>);
NEI.hide(<TwilightForest:tile.TFPlant:*>);
NEI.hide(<TwilightForest:tile.TFBossSpawner:*>);
NEI.hide(<TwilightForest:tile.TFTowerTranslucent:*>);
NEI.hide(<TwilightForest:tile.TFTrophy>);
NEI.hide(<TwilightForest:tile.ForceField:*>);
NEI.hide(<TwilightForest:item.tfspawnegg:*>);

NEI.hide(<WarpTheory:blockVanish>);

NEI.hide(<chisel:amber>);
NEI.hide(<chisel:bloodBrick:0>);

NEI.hide(<reccomplex:generic_space:*>);
NEI.hide(<reccomplex:generic_solid:*>);
NEI.hide(<reccomplex:weighted_command_block>);
NEI.hide(<reccomplex:inventory_generation_tag>);
NEI.hide(<reccomplex:inventory_generation_single_tag>);
NEI.hide(<reccomplex:inventory_generation_component_tag>);

NEI.hide(<thaumcraftneiplugin:Aspect>);

NEI.hide(<thaumicbases:pyrofluid:*>);

NEI.hide(<technom:fakeAirLight>);
NEI.hide(<technom:itemMaterial:4>);

NEI.hide(<AWWayofTime:bloodLight>);
NEI.hide(<AWWayofTime:lifeEssence>);
NEI.hide(<AWWayofTime:blockSchemSaver>);
NEI.hide(<AWWayofTime:blockMimic>);
NEI.hide(<AWWayofTime:spectralContainer>);

NEI.hide(<ForbiddenMagic:MobCrystal>);
