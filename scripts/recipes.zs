furnace.addRecipe(<minecraft:leather>, <minecraft:rotten_flesh>, 0.5);

recipes.addShapeless(<minecraft:stonebrick:1> * 1, [<minecraft:stonebrick:0>, <minecraft:vine>]);
recipes.addShapeless(<minecraft:stonebrick:1> * 1, [<minecraft:stonebrick:0>, <BiomesOPlenty:ivy>]);
recipes.addShapeless(<minecraft:stonebrick:1> * 1, [<minecraft:stonebrick:0>, <BiomesOPlenty:flowerVine>]);
recipes.addShapeless(<minecraft:stonebrick:1> * 1, [<minecraft:stonebrick:0>, <BiomesOPlenty:willow>]);

mods.thaumcraft.Arcane.addShapeless("ASPECTS", <TwilightForestPortalCatalyst:TwilightForestPortalCatalyst>, "ordo 8, aer 8, terra 8", [<minecraft:diamond>, <Thaumcraft:blockCustomPlant:1>]);

mods.avaritia.ExtremeCrafting.addShaped(<WarpTheory:item.warptheory.amulet>, [
	[<Thaumcraft:ItemResource:15>, null, null, null, <Thaumcraft:ItemEldritchObject:3>, null, null, null, <Thaumcraft:ItemResource:15>],
	[null, <Thaumcraft:ItemResource:15>, null, null, null, null, null, <Thaumcraft:ItemResource:15>, null],
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, <Thaumcraft:blockMirror:0>, null, null, null, null],
	[<Thaumcraft:ItemEldritchObject:3>, null, null, <Thaumcraft:blockMirror:0>, <Thaumcraft:ItemBaubleBlanks>, <Thaumcraft:blockMirror:0>, null, null, <Thaumcraft:ItemEldritchObject:3>],
	[null, null, null, null, <Thaumcraft:blockMirror:0>, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[null, <Thaumcraft:ItemResource:15>, null, null, null, null, null, <Thaumcraft:ItemResource:15>, null],
	[<Thaumcraft:ItemResource:15>, null, null, null, <Thaumcraft:ItemEldritchObject:3>, null, null, null, <Thaumcraft:ItemResource:15>]
]);

mods.avaritia.ExtremeCrafting.addShaped(<WarpTheory:item.warptheory.cleanser>, [
	[<Thaumcraft:blockCrystal:6>, null, null, null, <Thaumcraft:ItemEldritchObject:3>, null, null, null, <Thaumcraft:blockCrystal:6>],
	[null, <Thaumcraft:blockCrystal:6>, null, null, null, null, null, <Thaumcraft:blockCrystal:6>, null],
	[null, null, <Thaumcraft:blockCrystal:6>, null, null, null, <Thaumcraft:blockCrystal:6>, null, null],
	[null, null, null, <Thaumcraft:blockCrystal:6>, null, <Thaumcraft:blockCrystal:6>, null, null, null],
	[<Thaumcraft:ItemEldritchObject:3>, null, null, null, <minecraft:nether_star>, null, null, null, <Thaumcraft:ItemEldritchObject:3>],
	[null, null, null, <Thaumcraft:blockCrystal:6>, null, <Thaumcraft:blockCrystal:6>, null, null, null],
	[null, null, <Thaumcraft:blockCrystal:6>, null, null, null, <Thaumcraft:blockCrystal:6>, null, null],
	[null, <Thaumcraft:blockCrystal:6>, null, null, null, null, null, <Thaumcraft:blockCrystal:6>, null],
	[<Thaumcraft:blockCrystal:6>, null, null, null, <Thaumcraft:ItemEldritchObject:3>, null, null, null, <Thaumcraft:blockCrystal:6>]
]);
